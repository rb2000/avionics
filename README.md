# Avionics

For the project-oriented integrated course "Project Space Systems " at the Space Science Institute at the Technical University Berlin I was responsible for the Telemetry module. This presentation shows the Ground Station software I developed, using Spring Boot and WebSockets.  

The Ground Station sent requests to a server I programmed and was installed in an minicomputer(ODROID) inside the rocket's payload bay. A 400-G accelerometer connected to an ATMEGA2650 fed data to the Board Computer, which responded to the requests from the Ground Control Station.  Other sensors where also used ( vibration, loudness,temperature).  

It was also possible to establish a GeoFence ( a radius from a central coordinate), and to set the onboard computer to activate a relais in case that geofence is trespassed.  The microcontroller was connected to 2 Servos, so that trajectory corrections would also have been possible.

